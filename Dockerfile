FROM node:19-alpine

ENV MONGO_DB_USERNAME = admin \
    MONGO_DB_PWD=password

RUN mkdir -p /home/droneAPI

COPY . /Users/styles/Programming/DroneAPI2

CMD ["node", "/Users/styles/Programming/DroneAPI2/Drones/server.js"]